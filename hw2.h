#define MyField(i,j)  *(myField + (i-sx+hs) + (j-sy+hs)*(ex-sx+1+2*hs) )
#define SmallField(i,j)  *(smallField + (i-auxsx+hs) + (j-auxsy+hs)*(auxex-auxsx+1+2*hs) )
#define GlobField(i,j)  *(globField + i + (j)*(discr+1) )
#define HaloField(i,j)  *(myField + (i-sx) + (j-auxsy)*(auxex-auxsx+1) )

#define Lasts(j,k) 	*(myField + (auxex-auxsx+1+k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Lastr(j,k) 	*(myField + (auxex-auxsx+auxhs+1+k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Firsts(j,k) 	*(myField + (k + auxhs) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Firstr(j,k) 	*(myField + (k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )



typedef struct Maps{
	int sx,ex,sy,ey;
	int hs;//Halo size
	//mida de tots
}MAP;

void worksplit( int*, int*, int, int, int, int);
void FillField(MAP *mymap, double* myField);
void Fill0(MAP *mymap, double* myField);

void checkr(int r,char *txt);
int quisoc();
int quants();
int quisocx(int NPX);
int quisocy(int NPX);
void CreateMAP(int NPX, int NPY, int gsx, int gex, int gsy, int gey, MAP *mymap, int has);
void PrintMAP(MAP *mymap);

double *AllocField(MAP *mymap);

void PrintField(MAP *mymap, double* myField);

void PrintGlobal(MAP *mymap, int NPX, int NPY, double* myField, int discr);

void HaloUpdate(MAP *mymap, double* myField, int NPX, int NPY);
void HaloY(MAP *mymap, double* myField, int NPX, int NPY);
void HaloX(MAP *mymap, double* myField, int NPX, int NPY);

double* GetLastVec(MAP *mymap, double* myField);
double* GetFirstVec(MAP *mymap, double* myField);
void SetLastVec(MAP *mymap, double* myField, double* retl);
void SetFirstVec(MAP *mymap, double* myField, double* retl);
