#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "hw2.h"

#define MyField(i,j)  *(myField + (i-sx+hs) + (j-sy+hs)*(ex-sx+1+2*hs) )
#define SmallField(i,j)  *(smallField + (i-auxsx+hs) + (j-auxsy+hs)*(auxex-auxsx+1+2*hs) )
#define GlobField(i,j)  *(globField + i + (j)*(discr+1) )
#define HaloField(i,j)  *(myField + (i-sx) + (j-auxsy)*(auxex-auxsx+1) )

//Macro Punt(linia)   *(ultim punt o primer punt)
#define Lasts(j,k) 	*(myField + (auxex-auxsx+1+k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Lastr(j,k) 	*(myField + (auxex-auxsx+auxhs+1+k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Firsts(j,k) 	*(myField + (k + auxhs) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )
#define Firstr(j,k) 	*(myField + (k) + (j-auxsy+auxhs)*(auxex-auxsx+1+2*auxhs) )


void HaloUpdate(MAP *mymap, double* myField, int NPX, int NPY){

	HaloY(mymap, myField, NPX, NPY);
	PrintField(mymap,myField);
	HaloX(mymap, myField, NPX, NPY);

}

void checkr(int r,char *txt) {
	if (r!=MPI_SUCCESS) {
		fprintf(stderr,"Error: %s\n",txt);
		exit(-1);
	}
}

int quisoc() {
	int a,b;
	a=MPI_Comm_rank(MPI_COMM_WORLD,&b);
	checkr(a,"quisoc");
	return(b);
}

int quisocx(int NPX) {
	int ret = quisoc()%NPX;
	return(ret);
}

int quisocy(int NPX) {
	int ret = quisoc()/NPX;
	return(ret);
}


int quants() {
	int a,b;
	a=MPI_Comm_size(MPI_COMM_WORLD,&b);
	checkr(a,"quants");
	return(b);
}
// Macro to access eld u
// de ne U(i,j) *( u + ... (map->sx)...)
// de ne V(i,j)

void CreateMAP(int NPX, int NPY, int gsx, int gex, int gsy, int gey, MAP *mymap, int has){
	int startx;
	int endx;
	int starty;
	int endy;
	worksplit(&startx, &endx, quisoc()%NPX, NPX, gsx, gex);//Modul columna
	worksplit(&starty, &endy, quisoc()/NPX, NPY, gsy, gey);//Divisio fila

	mymap->sx = startx;
	mymap->ex = endx;
	mymap->sy = starty;
	mymap->ey = endy;
	mymap->hs = has;
}
void PrintMAP(MAP *mymap){
	printf("Processador: %d     sx  %d   ex %d    sy%d   ey%d \n", quisoc(), mymap->sx, mymap->ex, mymap -> sy, mymap->ey);

}
double *AllocField(MAP *mymap){

	double* myField = (double*) malloc(((mymap->ex - mymap->sx+1 + 2*mymap->hs)* //Continues
		(mymap->ey - mymap->sy+1 + 2*mymap->hs)* sizeof(double)));
	if(myField ==NULL){
		printf("Error, allocation unsuccessful");
		exit(EXIT_FAILURE);
	}
	return myField;
}

void FillField(MAP *mymap, double* myField){
	int sx = mymap->sx;
	int ex = mymap->ex;
	int sy = mymap->sy;
	int ey = mymap->ey;
	int hs = mymap->hs;
	for (int j = ey; j>=sy; j--){
		for(int i = sx; i <=ex; i++){
				//MyField(i,j) = CalcMandel(i,j,discr, realinf, realsup, iminf, imsup); //per imprimir per files la matriu domini
				MyField(i,j) = (i*100 + j); //perque ens doni el numero i veure si halo esta be
			}

		}
	}
	void Fill0(MAP *mymap, double* myField){
		int sx = mymap->sx;
		int ex = mymap->ex;
		int sy = mymap->sy;
		int ey = mymap->ey;
		int hs = mymap->hs;
		for (int j = ey+hs; j>=sy-hs; j--){
			for(int i = sx-hs; i <=ex+hs; i++){
				MyField(i,j) = 0; //perque ens doni el numero i veure si halo esta be
			}

		}
	}
//void FillHalo(MAP *mymap, double* myField, int *recvsx){
//	int sx = mymap->sx;
//	int ex = mymap->ex;
//	int sy = mymap->sy;
//	int ey = mymap->ey;
//	int hs = mymap->hs;
////for (int j= ey; j>=sy; j--){
//	for(int i= sx; i<=ex, i++){
//		HaloField(i,j)= *(recvsx+1);
//	}
//}


	void PrintField(MAP *mymap, double* myField){
		int sx = mymap->sx;
		int ex = mymap->ex;
		int sy = mymap->sy;
		int ey = mymap->ey;
		int hs = mymap->hs;
		for (int j = ey+hs; j>=sy-hs; j--){
			printf("p = %d j = %d ::",quisoc(),j);
			for(int i = sx-hs; i <=ex+hs ; i++){
				printf(" %.0f ", MyField(i,j));
				//domini(i,j) =
			}
		printf("\n"); //salt de fila quan acaba j
	}
		printf("p = %i \n",quisoc()); //salt de fila quan acaba j
	}





	void PrintGlobal(MAP *mymap, int NPX, int NPY, double* myField, int discr){

		int sx = mymap->sx;
		int ex = mymap->ex;
		int sy = mymap->sy;
		int ey = mymap->ey;
		int hs = mymap->hs;
	MPI_Status st; /* allows to check the tag (if needed) */
		int r;
		if(quisoc()!=0){
			int l[5]={mymap->sx,mymap->ex,mymap->sy,mymap->ey,mymap->hs};
			r = MPI_Ssend(l,5,MPI_INT,0,0,MPI_COMM_WORLD);

			r = MPI_Ssend(myField,(ex-sx+1)*(ey-sy+1),MPI_DOUBLE,0,4,MPI_COMM_WORLD);

		}
		else {

			MAP globMap;
			MAP smallMap;
			globMap.sx = 0;
			globMap.ex = discr;
			globMap.sy = 0;
			globMap.ey = discr;
			globMap.hs = 0;
			int auxsx;
			int auxex;
			int auxsy;
			int auxey;
			int auxhs;
			double* globField = (double*) malloc((discr+1)*(discr+1)*sizeof(double));

			if(globField ==NULL){
				printf("Error, allocation unsuccessful");
			}

			int aux[5];
			for (int rank = 1;rank<NPX*NPY;rank++){

				r = MPI_Recv(aux,5,MPI_INT,rank,0,MPI_COMM_WORLD,&st);
				checkr(r,"send sx");
				smallMap.sx=aux[0];
				smallMap.ex=aux[1];
				smallMap.sy=aux[2];
				smallMap.ey=aux[3];
				smallMap.hs=aux[4];
				auxsx=aux[0] ;
				auxex=aux[1] ;
				auxsy=aux[2] ;
				auxey=aux[3] ;
				auxhs=aux[4] ;
			double* smallField = (double*) malloc(((auxex - auxsx+1)* //Continues
				(auxey - auxsy+1)* sizeof(double)));//afe

			if (smallField == NULL) {
				printf("Error, allocation unsuccessful\n");
			}
			r = MPI_Recv(smallField,(auxex - auxsx+1)* //Continues
				(auxey - auxsy+1),MPI_DOUBLE,rank,4,MPI_COMM_WORLD,&st);
			//printf("P %d %d %d %d  %d \n",rank,auxsx,auxex,auxsy,auxey);

			for (int i = auxsx;i<=auxex;i++){
				for (int j = auxsy; j <= auxey; j++)
				{
					//printf("valor de proc %d ,i%d, j %d, %f \n",rank,i,j,SmallField(i,j));
					GlobField(i,j) = SmallField(i,j);
				}

			}

			free(smallField);
		}

		for (int i = sx;i<=ex;i++){
			for (int j = sy; j <= ey; j++)
			{
					//printf("valor de proc %d ,i%d, j %d, %f \n",0,i,j,MyField(i,j));
				GlobField(i,j) = MyField(i,j);
			}

		}
		PrintField(&globMap, globField);

		free(globField);
	}

}


void worksplit( int* mystart, int* myend, int nproc, int proc , int start , int end){
	int info = 0;
	if (nproc >= proc) {
		printf("Warning! Processor number higher than available. \n");
		exit(0);
	}
	if (start > end) {
		printf("Warning! Wrong input \n");
		exit(0);
	}
	int entero = (end - start+1)/proc;
	int resto = (end - start+1)%proc;

	if (nproc<resto){
		*mystart = start +  (nproc)*(entero+1);
		*myend = *mystart + (entero);
	}
	if(nproc>=resto){
		*mystart = start + resto * (entero + 1) + (nproc-resto)*entero;
		*myend = *mystart + entero-1;
	}

}


void HaloY(MAP *mymap, double* myField, int NPX, int NPY){
	int info = 0;
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;

	int recvsx;

	MPI_Status st;


	if (quisocy(NPX)%2 == 0){//0,2......

		int fila = (quisocy(NPX)+1)%NPY;
		int columna = quisocx(NPX);
		int desti = fila * NPX + columna;
		double *posini = myField+(auxey-auxsy+1)*(auxex-auxsx+2*auxhs+1);
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;
		if (info)
			printf("haloy : desti %i , posini %f , mida %i \n", desti, *posini, mida);

		MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera nord amunt

	}
	else {//1,3.....
		int fila = quisocy(NPX)-1;
		int columna = quisocx(NPX);
		int origen = fila * NPX + columna;

		double *posini = myField;
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;

		MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera sud de baix

	}

	if (quisocy(NPX)%2 != 0){//1,3

		int fila = quisocy(NPX)-1;
		int columna = quisocx(NPX);
		int desti = fila * NPX + columna;
		double *posini = myField+(auxex-auxsx+2*auxhs+1)*auxhs;
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;
		if (info)
			printf("desti %i , posini %f , mida %i \n", desti, *posini, mida);

		MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera sud avall


			//r = MPI_Ssend(l,5,MPI_INT,0,0,MPI_COMM_WORLD);

			//r = MPI_Ssend(myField,(ex-sx+1)*(ey-sy+1),MPI_DOUBLE,0,4,MPI_COMM_WORLD);
	}
	else {//0,2
		int fila = (quisocy(NPX)+1)%NPY;
		int columna = quisocx(NPX);
		int origen = fila * NPX + columna;
		double *posini = myField+(auxey-auxsy+auxhs+1)*(auxex-auxsx+2*auxhs+1);
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;

		MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera nord de dalt

	}
	//*******************  Sentit contrari ***************************/

	if (quisocy(NPX)%2 != 0){//1,3

		int fila = (quisocy(NPX)+1)%NPY;
		int columna = quisocx(NPX);
		int desti = fila * NPX + columna;
		double *posini = myField+(auxey-auxsy+1)*(auxex-auxsx+2*auxhs+1);
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;
		if (info)
			printf("desti %i , posini %f , mida %i \n", desti, *posini, mida);

		MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera nord amunt


		}
	else {//0,2....
		
			int fila = (NPY+quisocy(NPX)-1)%NPY;
			int columna = quisocx(NPX);
			int origen = fila * NPX + columna;
			double *posini = myField;
			int mida = (auxex-auxsx+2*auxhs+1) *auxhs;

			MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera sud de baix

	}

	if (quisocy(NPX)%2 == 0){//0,2....

			int fila = (NPY+quisocy(NPX)-1)%NPY;
			int columna = quisocx(NPX);
			int desti = fila * NPX + columna;
			double *posini = myField+(auxex-auxsx+2*auxhs+1)*auxhs;
			int mida = (auxex-auxsx+2*auxhs+1) *auxhs;
			if (info)
				printf("desti %i , posini %f , mida %i \n", desti, *posini, mida);

			MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera sud avall


	}
	else {//1,3.....

		int fila = (quisocy(NPX)+1)%NPY;
		int columna = quisocx(NPX);
		int origen = fila * NPX + columna;
		double *posini = myField+(auxey-auxsy+auxhs+1)*(auxex-auxsx+2*auxhs+1);
		int mida = (auxex-auxsx+2*auxhs+1) *auxhs;

			MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera nord de dalt
		}


	}
void HaloX (MAP *mymap, double* myField, int NPX, int NPY){
	int info =0;
	//	First es oest esquerra i last est dreta
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;

	int recvsx;

	MPI_Status st;

	if (quisocx(NPX)%2 == 0){//0,2

		int fila = quisocy(NPX);
		int columna = (quisocx(NPX)+1)%NPX;
		int desti = columna + NPX * fila;
		double *posini = GetLastVec(mymap,myField);

		int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
		//printf("estem en x desti %i , posini %f , mida %i \n", desti, *posini, mida);

		MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera est
		if (info)
		printf("Soc proc %d i he enviat a %d \n",quisoc(),desti);

		//free(posini);
	}
	else {//1,3
		int fila = quisocy(NPX);
		int columna = (NPX+quisocx(NPX)-1)%NPX;
		int origen = columna + NPX*fila;


		int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
		double *posini = (double*) malloc(mida * sizeof(double));
		if(posini ==NULL){
		printf("Error, allocation unsuccessful");
		exit(EXIT_FAILURE);
		}

		MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera oest esquerra
		if (info)
		printf("Soc proc %d i he rebut de %d \n",quisoc(),origen);
		SetFirstVec(mymap,myField,posini);

		free(posini);
	}
	if (quisocx(NPX)%2 != 0){//1,3
		int fila = quisocy(NPX);
		int columna = (NPX+quisocx(NPX)-1)%NPX;
		int desti = columna + NPX * fila;
		
		double *posini = GetFirstVec(mymap,myField);
		int mida = (auxey-auxsy+2*auxhs+1) * auxhs;
		
		MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera oest esquerra
		
		if (info)
		printf("Soc proc %d i he enviat a %d \n",quisoc(),desti);

		//free(posini);
	}
	else {//0,2

		int fila = quisocy(NPX);
		int columna = (NPX+quisocx(NPX)+1)%NPX;
		int origen = fila * NPX + columna;
		int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
		double *posini = (double*) malloc(mida * sizeof(double));
		if(posini ==NULL){
		printf("Error, allocation unsuccessful");
		exit(EXIT_FAILURE);
		}

		MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera est dreta
		if (info)
		printf("Soc proc %d i he rebut de %d \n",quisoc(),origen);

		SetLastVec(mymap,myField,posini);
		free(posini);

	}
	//*******************  Sentit contrari ***************************/

	if (quisocx(NPX)%2 != 0){//1,3 columnaaaa

		int fila = quisocy(NPX);
		int columna = (NPX+quisocx(NPX)+1)%NPX;
		int desti = fila * NPX + columna;
		double *posini = GetLastVec(mymap,myField);
		int mida = (auxey-auxsy+2*auxhs+1)*auxhs;

			MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera est dreta


	}
	
	else {//0,2 columneeees

			int fila = quisocy(NPX);
			int columna = (NPX+quisocx(NPX)-1)%NPX;
			int origen = fila * NPX + columna;
			int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
			double *posini = (double*) malloc(mida * sizeof(double));
			if(posini ==NULL){
				printf("Error, allocation unsuccessful");
				exit(EXIT_FAILURE);
			}

			MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera oest esquerra
			if (info)
				printf("Soc proc %d i he rebut de %d \n",quisoc(),origen);
			SetFirstVec(mymap,myField,posini);
	}

	if (quisocx(NPX)%2 == 0){//0,2

			int fila = quisocy(NPX);
			int columna = (NPX+quisocx(NPX)-1)%NPX;
			int desti = fila * NPX + columna;
			double *posini = GetFirstVec(mymap,myField);
			int mida = (auxey-auxsy+2*auxhs+1)*auxhs;

			MPI_Ssend(posini,mida, MPI_DOUBLE,desti,2,MPI_COMM_WORLD);//Envia frontera oest esquerra

	}
	
	else {//1,3

		int fila = quisocy(NPX);
		int columna = (NPX+quisocx(NPX)+1)%NPX;
		int origen = fila * NPX + columna;
		int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
		double *posini = (double*) malloc(mida * sizeof(double));
		if(posini ==NULL){
			printf("Error, allocation unsuccessful");
			exit(EXIT_FAILURE);
		}

		MPI_Recv(posini,mida, MPI_DOUBLE, origen,2, MPI_COMM_WORLD, &st);//Rep frontera est dreta
		if (info)
			printf("Soc proc %d i he rebut de %d \n",quisoc(),origen);
		SetLastVec(mymap,myField,posini);
	}




}

double* GetLastVec(MAP *mymap, double* myField){
	int info = 0;
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;
	int mida = (auxey-auxsy+2*auxhs+1)*auxhs;
	
	double* retl = (double*) malloc( mida * sizeof(double));
	if(retl ==NULL){
		printf("Error, allocation unsuccessful");
		exit(EXIT_FAILURE);
	}
	int i = 0;
	for (int k = 0; k < auxhs; k++){
		for (int j = auxsy-auxhs;j<=auxey+auxhs;j++){
	
			retl[i] = Lasts(j,k);
			if (info)
			printf(" last p = %i, j = %i envio %f\n",quisoc(),j,retl[i]); //he canviat retl[j] per retl[i]
			i++;
		}
	}

	return retl;

}

void SetLastVec(MAP *mymap, double* myField, double* retl){
	int info = 0;
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;
	int i = 0;
	for (int k = 0; k < auxhs; k++){
		for (int j = auxsy-auxhs;j<=auxey+auxhs;j++){
	
			Lastr(j,k) = retl[i];
			i++;
			if (info)
			printf(" last p = %i, j = %i rebo %f\n",quisoc(),j,retl[i]); //he canviat retl[j] per retl[i]
	
		}
	}

}

double* GetFirstVec(MAP *mymap, double* myField){
	int info = 0;
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;
	int mida = (auxey-auxsy+2*auxhs+1)*auxhs;

	double* retf = (double*) malloc(mida * sizeof(double));
	if(retf ==NULL){
		printf("Error, allocation unsuccessful");
		exit(EXIT_FAILURE);
	}
	int i = 0;
	for (int k = 0; k < auxhs; k++){
		for (int j = auxsy-auxhs;j<=auxey+auxhs;j++){
	
			retf[i] = Firsts(j,k);
			if (info)
			printf(" first p = %i, j = %i envio inic %f\n",quisoc(),j,retf[i]); //he canviat retl[j] per retl[i]
			i++;
		}
	}
	return retf;

}
void SetFirstVec(MAP *mymap, double* myField, double* retl){
	int info = 0;
	int auxex = mymap->ex;
	int auxsx = mymap->sx;
	int auxsy = mymap->sy;
	int auxey = mymap->ey;
	int auxhs = mymap->hs;
	int i = 0;
	for (int k = 0; k < auxhs; k++){
		for (int j = auxsy-auxhs;j<=auxey+auxhs;j++){
	
			Firstr(j,k) = retl[i];
			i++;
			if (info)
			printf(" first p = %i, j = %i rebo %f\n",quisoc(),j,Firstr(j,k)); //he canviat retl[j] per retl[i]
	
		}
	}

}
