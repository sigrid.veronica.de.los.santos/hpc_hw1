#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#include "hw2.h"
int main(int argc, char** argv){

		int r = MPI_Init(&argc,&argv);

		checkr(r, "init");


	MAP mymap;
	int NPY = 2;
	int NPX = 2;
		int hs = 2;
	int gsx = hs;
	int gex = 10;
	int gey = 10;
	int gsy = hs;



	CreateMAP(NPX,NPY,gsx,gex,gsy,gey,&mymap,hs);
	PrintMAP(&mymap);
	double* myField = AllocField(&mymap);
	Fill0(&mymap,myField);
	PrintField(&mymap,myField);
	FillField(&mymap,myField);

	PrintField(&mymap,myField);

	HaloUpdate(&mymap,myField,NPX, NPY);
	
	PrintField(&mymap,myField);
	//SetLastVec(&mymap,myField, GetLastVec(&mymap,myField));
	//SetFirstVec(&mymap,myField, GetFirstVec(&mymap,myField));


	//PrintField(&mymap,myField);


	free(myField);
	
	MPI_Finalize();
	return 0;


}
